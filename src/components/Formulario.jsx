import React from 'react'
import { useState, useEffect } from 'react'
import Error from './Error'

const Formulario = ({ paciente, pacientes, setPacientes, setPaciente }) => {

    const [nombre, setNombre] = useState("")
    const [propietario, setPropietario] = useState("")
    const [email, setEmail] = useState("")
    const [alta, setAlta] = useState("")
    const [sintomas, setSintomas] = useState("")
    const [error, setError] = useState(false)

    useEffect( () => {
        if (Object.keys(paciente).length > 0){
            setNombre(paciente.nombre)
            setPropietario(paciente.propietario)
            setEmail(paciente.email)
            setAlta(paciente.alta)
            setSintomas(paciente.sintomas)
        }   
    },[paciente])

    const handleSubmit = (e) => {
        e.preventDefault()
        if ( [nombre, propietario, email, alta, sintomas].includes("") ){
            setError(true)
            return;
        }
        setError(false)

        const generarID = () => {
            const random = Math.random().toString(36).substring(2)
            const fecha = Date.now().toString(36)
            return random + fecha 
        }

        const objPaciente = {
            nombre,
            propietario,
            email,
            alta,
            sintomas,
            id: generarID()
        }

        if(paciente.id){
            // Editando Registro
            objPaciente.id = paciente.id
            const pacientesActualizados = pacientes.map( pacienteState => 
                paciente.id === pacienteState.id ? objPaciente : pacienteState
            )
            setPacientes(pacientesActualizados)
        }
        else{
            // Ingresando Nuevo Registro
            objPaciente.id = generarID()
            setPacientes([...pacientes, objPaciente])
        }

        // Reiniciar Form
        setNombre("")
        setPropietario("")
        setEmail("")
        setAlta("")
        setSintomas("")
        setPaciente({})
         
    }

    return (
        <div className="md:w-1/2 lg:w-2/5" >
            <h2 className='font-black text-3xl text-center'>Seguimeinto Pacientes</h2>

            <p className = "text-lg mt-5 text-center mb-10">
                Añade Pacientes y {""}
                <span className='text-indigo-600 font-bold'>Administralos</span>
            </p>

            <form   onSubmit={ handleSubmit }  
                    className='bg-white shadow-md rounded-lg py-10 px-5 mb-5 hover:bg-indigo-50'>
                <div className = "mb-5">
                    <label htmlFor = "mascota" className='block text-gray-700 uppercase font-bold hover:text-gray-500'>
                        Nombre Mascota
                    </label>
                    <input
                        id = "mascota" 
                        type = "text"
                        placeholder='Nombre de la Mascota'
                        value={nombre}
                        onChange= { (e) => setNombre(e.target.value) }
                        className='border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md'
                    />
                </div>
                <div className = "mb-5">
                    <label htmlFor = "propietario" className='block text-gray-700 uppercase font-bold hover:text-gray-500'>
                        Nombre Propietario
                    </label>
                    <input
                        id = "propietario" 
                        type = "text"
                        placeholder='Nombre del Propietario'
                        value={propietario}
                        onChange= { (e) => setPropietario(e.target.value) }
                        className='border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md'
                    />
                </div>
                <div className = "mb-5">
                    <label htmlFor = "email" className='block text-gray-700 uppercase font-bold hover:text-gray-500'>
                        Email
                    </label>
                    <input
                        id = "email" 
                        type = "email"
                        placeholder='Email Contacto Propietario'
                        value={email}
                        onChange= { (e) => setEmail(e.target.value) }
                        className='border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md'
                    />
                </div>
                <div className = "mb-5">
                    <label htmlFor = "alta" className='block text-gray-700 uppercase font-bold hover:text-gray-500'>
                        Alta
                    </label>
                    <input
                        id = "alta" 
                        type = "date"
                        value={alta}
                        onChange= { (e) => setAlta(e.target.value) }
                        className='border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md cursor-pointer'
                    />
                </div>
                <div className = "mb-5">
                    <label htmlFor = "sintomas" className='block text-gray-700 uppercase font-bold hover:text-gray-500'>
                        Síntomas
                    </label>
                    <textarea
                        id = "sintomas"
                        placeholder = "Describe los Síntomas"
                        value={sintomas}
                        onChange= { (e) => setSintomas(e.target.value) }
                        className='border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md h-32'
                    />
                </div>
                <input
                    type = "submit"
                    className=' bg-indigo-600 w-full p-3 text-white uppercase font-bold
                                hover:bg-indigo-800 hover:text-cyan-700 cursor-pointer transition-colors'
                    value = { paciente.id ? "Editar Paciente":"Agregar Paciente"}
                />
                { error && <Error mensaje = "Todos los Campos Son Obligatorios"/>}
                
            </form>

        </div>    
    )
}

export default Formulario
