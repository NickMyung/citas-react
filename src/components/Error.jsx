import React from 'react'

const Error = ({mensaje}) => {
  return (
    <div className='text-center text-red-600 font-semibold text-xs'>
        <p>{mensaje}</p>
    </div>
  )
}

export default Error