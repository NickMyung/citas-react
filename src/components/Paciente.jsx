import React from 'react'

const Paciente = ({ paciente, setPaciente, eliminarPaciente }) => {

    const { nombre, propietario, email, alta, sintomas, id } = paciente

    const handleEliminar = () => {
        const respuesta = confirm("Desea Eliminar Este Paciente?");
        
        if(respuesta){
            eliminarPaciente(id)
        }
    }

  return (
    <div className=' ml-5 mb-3 mr-1 bg-white shadow-md px-5 py-8 rounded-xl hover:bg-indigo-50'>
        <p className=' font-bold mb-3 text-gray-700 uppercase'>Nombre Mascota: {""}
            <span className='font-normal normal-case'>{nombre}</span>
        </p>
        <p className=' font-bold mb-3 text-gray-700 uppercase'>Nombre Propietario: {""}
            <span className='font-normal normal-case'>{propietario}</span>
        </p>
        <p className=' font-bold mb-3 text-gray-700 uppercase'>Email: {""}
            <span className='font-normal normal-case'>{email}</span>
        </p>
        <p className=' font-bold mb-3 text-gray-700 uppercase'>Fecha Alta: {""}
            <span className='font-normal normal-case'>{alta}</span>
        </p>
        <p className=' font-bold mb-3 text-gray-700 uppercase'>Síntomas: {""}
            <span className='font-normal normal-case'>{sintomas}</span>
        </p>

        <div className=' flex justify-between mt-8'>
            <button type='button' className='py-2 px-10 bg-indigo-600 hover:bg-indigo-800 
                                           text-white font-bold uppercase rounded-md hover:text-red-600'
                    onClick={ () => setPaciente(paciente) }>
                Editar
            </button>
            <button type='button' className='py-2 px-10 bg-red-600 hover:bg-red-800 
                                           text-white font-bold uppercase rounded-md hover:text-indigo-600'
                    onClick={ handleEliminar }>
                Eliminar
            </button>
        </div>
    </div>
  )
}

export default Paciente
